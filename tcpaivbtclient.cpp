#include "tcpaivbtclient.h"

TcpAivbtClient::TcpAivbtClient(QHostAddress host, int port, QObject *parent)
    : QObject(parent), _hostIp(host), _port(port)
{
    connect(&_tcpSocket, &QTcpSocket::connected, this, &TcpAivbtClient::onDisplayConnected);
}

void TcpAivbtClient::writeMessage(int numDisplay
    , int numMsg
    , QList<QString> lines
    )
{
    QTextStream sstream(&_bufOut);

    // Préparer le message à envoyer à la prochaine connexion :
    // * ID trame (0x03=STX)
    sstream << QChar(0x02);
    sstream.flush();

    // * Adresse afficheur sur 3 octets en ASCII
    sstream.setFieldWidth(3);
    sstream.setFieldAlignment(QTextStream::AlignRight);
    sstream.setPadChar('0');
    sstream << numDisplay;
    // OU
    //sstream << qSetFieldWidth(3) << qSetPadChar('0') << numDisplay;
    // OU
    //sstream        << QString("%1").arg(numDisplay,3,10,QChar('0'));
    sstream.flush();
    sstream.reset();

    // * Séparateur
    sstream << QChar(0x1D);

    // * Numéro de message sur 3 octets en ASCII
    sstream.setFieldWidth(3);
    sstream.setFieldAlignment(QTextStream::AlignRight);
    sstream.setPadChar('0');
    sstream << numMsg;
    sstream.flush();
    sstream.reset();

    // * Séparateur
    sstream << QChar(0x1D);
    sstream.flush();

    // * Lignes de messages (4x40 caractères)
    for(const auto& line : lines) {
        sstream << QString("%1").arg(line, -40, ' ');
    }
    sstream.flush();

    // * Séparateur
    sstream << QChar(0x1D);
    sstream.flush();

    // * Heures de début et de fin
    sstream << "06002200";
    sstream.flush();

    // * Séparateur
    sstream << QChar(0x1D);
    sstream.flush();

    // * Dates de début et de fin
    sstream << QString(12, '-');
    sstream.flush();

    // * Séparateur
    sstream << QChar(0x1D);
    sstream.flush();

    // * Durée affichage sur 2 octets en ASCII
    sstream.setFieldWidth(2);
    sstream.setFieldAlignment(QTextStream::AlignRight);
    sstream.setPadChar('0');
    sstream << 5;
    sstream.flush();
    sstream.reset();

    // * Fin de trame (0x03=ETX)
    sstream << QChar(0x03);
    sstream.flush();

    // Mettre fin à toute connexion en cours
    _tcpSocket.abort();

    // Lancer la connexion
    _tcpSocket.connectToHost(_hostIp, _port);
}

void TcpAivbtClient::eraseAllMessages(int numDisplay)
{
    _bufOut.clear();

    // Préparer le message à envoyer :
    // * trame sans adresse (ID=0x01=SOH)
    _bufOut.append(QByteArray("\x01\x55\xAA\x04"));

    // * Adresse afficheur sur 1 octet
    _bufOut.insert(1, numDisplay);

    // Mettre fin à toute connexion en cours
    _tcpSocket.abort();

    // Lancer la connexion
    _tcpSocket.connectToHost(_hostIp, _port);
}

void TcpAivbtClient::updateRTC(int numDisplay, QDateTime date)
{
    QTextStream sstream(&_bufOut);

    // Préparer le message à envoyer :
    // * ID trame (0x05=ENQ)
    sstream << QChar(0x05);

    // * Adresse afficheur
    sstream << QChar(numDisplay);

    // * Heure + Date + n° jour semaine
    QString format = "hhmmssMMddyy";
    sstream << date.toString(format) << date.date().dayOfWeek() % 7;

    // * Fin de trame (0x06=ACK)
    sstream << QChar(0x06);
    sstream.flush();

    // Mettre fin à toute connexion en cours
    _tcpSocket.abort();

    // Lancer la connexion
    _tcpSocket.connectToHost(_hostIp, _port);
}

void TcpAivbtClient::onDisplayConnected()
{
    // Effacer buffer de réception
    _bufIn.clear();

    // Envoyer le message contenu dans le buffer
    _tcpSocket.write(_bufOut);
}
