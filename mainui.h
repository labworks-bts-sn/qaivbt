#ifndef MAINUI_H
#define MAINUI_H

#include <QDialog>
#include "tcpaivbtclient.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainUI; }
QT_END_NAMESPACE

class MainUI : public QDialog
{
    Q_OBJECT

public:
    MainUI(QWidget *parent = nullptr);
    ~MainUI();

private slots:

    void on_chkbxSyncHorlogePC_stateChanged(int arg1);
    void onTmrTimeout();


    void on_btnConnect_clicked();

    void on_btnUpdateRTC_clicked();

    void on_btnErase_clicked();

    void on_btnSendMsg_clicked();

private:
    Ui::MainUI *ui;
    QTimer * _tmr;
    TcpAivbtClient * _display;
};
#endif // MAINUI_H
