# QAIVBT

Application Qt de pilotage de l'afficheur AIVBT de la société MAT Electronique.

![Afficheur AIVBT](img/aivbt-front.jpg  "Afficheur AIVBT")

Pour une description des différents constituants et des protocoles de communication voir [Documentation-AIVBT14](assets/AIVBT14-404%20(V1.01).pdf).
