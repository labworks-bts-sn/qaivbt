#ifndef TCPAIVBTCLIENT_H
#define TCPAIVBTCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDataStream>
#include <QDate>
#include <QTimer>

class TcpAivbtClient : public QObject
{
    Q_OBJECT
public:
    explicit TcpAivbtClient(QHostAddress host, int port, QObject *parent = nullptr);
    void writeMessage(int numDisplay, int numMsg, QList<QString> lines);
    void eraseAllMessages(int numDisplay);
    void updateRTC(int numDisplay, QDateTime date);

signals:
    void ackReceived();
    void errorOccured(QString err);

private slots:
    void onDisplayConnected();

private:
    QTcpSocket _tcpSocket;
    const QHostAddress _hostIp;
    const int _port;
    void handleError(QAbstractSocket::SocketError);
    QDataStream _in;
    QByteArray _bufOut;
    QByteArray _bufIn;
};

#endif // TCPAIVBTCLIENT_H
