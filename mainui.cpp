#include <QTimer>
#include <QHostAddress>
#include <QMessageBox>
#include <QList>
#include "mainui.h"
#include "ui_mainui.h"

MainUI::MainUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainUI)
{
    ui->setupUi(this);
}

MainUI::~MainUI()
{
    delete ui;
}

void MainUI::on_chkbxSyncHorlogePC_stateChanged(int arg1)
{

    if(arg1 == Qt::Checked) {
        ui->dedtDate->setEnabled(false);
        ui->tedtTime->setEnabled(false);
        _tmr = new QTimer();
        connect(_tmr, &QTimer::timeout, this, &MainUI::onTmrTimeout);
        _tmr->start();
        onTmrTimeout();
    } else {
        ui->dedtDate->setEnabled(true);
        ui->tedtTime->setEnabled(true);
        if(_tmr != nullptr) {
            _tmr->stop();
            delete _tmr;
        }
        onTmrTimeout();
    }
}

void MainUI::onTmrTimeout()
{
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();
    ui->dedtDate->setDate(date);
    ui->tedtTime->setTime(time);
}


void MainUI::on_btnConnect_clicked()
{
    QHostAddress host;
    int port;
    bool okIp;
    bool okPort;

    if(ui->btnConnect->isChecked()) {
        port = ui->ledtDisplayPort->text().toInt(&okPort);
        okIp = host.setAddress(ui->ledtDisplayIP->text());
        if(okPort && (port <= 1024 || port > 65535)) {
            okPort = false;
        }

        if( okIp && okPort) {
            _display = new TcpAivbtClient(host, port);
        } else {
            QMessageBox::warning(this, "Erreur", "Adresse IP et/ou Port non valides");
        }

        // MàJ IHM
        ui->btnConnect->setText("Déconnecter");
        ui->ledtDisplayIP->setEnabled(false);
        ui->ledtDisplayPort->setEnabled(false);
        ui->grpbxDefMsg->setEnabled(true);
        ui->grpbxDisplayAddr->setEnabled(true);
        ui->grpbxDisplayProg->setEnabled(true);
    } else {
        if(_display != nullptr) {
            delete _display;
            _display = nullptr;
        }
        // MàJ IHM
        ui->btnConnect->setText("Connecter");
        ui->ledtDisplayIP->setEnabled(true);
        ui->ledtDisplayPort->setEnabled(true);
        ui->grpbxDefMsg->setEnabled(false);
        ui->grpbxDisplayAddr->setEnabled(false);
        ui->grpbxDisplayProg->setEnabled(false);
    }

}


void MainUI::on_btnUpdateRTC_clicked()
{
    if(ui->chkbxSyncHorlogePC->isChecked()) {
        _display->updateRTC(ui->spbxNumDisplay->value(), QDateTime::currentDateTime());
    } else {
        QDateTime dt;
        dt.setDate(ui->dedtDate->date());
        dt.setTime(ui->tedtTime->time());
        _display->updateRTC(ui->spbxNumDisplay->value(), dt);
    }
}


void MainUI::on_btnErase_clicked()
{
    _display->eraseAllMessages(ui->spbxNumDisplay->value());
}

void MainUI::on_btnSendMsg_clicked()
{
    QList<QString> lines;

    lines.append(ui->ledtLine1->text());
    lines.append(ui->ledtLine2->text());
    lines.append(ui->ledtLine3->text());
    lines.append(ui->ledtLine4->text());

    _display->writeMessage(ui->spbxNumDisplay->value()
        , ui->spbxNumMsgToWrite->value()
        , lines
        );
}